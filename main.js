async function getTokens(tokenId) {
  const hex = tokenId.toString(16);
  const data = '0xd43500370000000000000000000000000000000000000000000000000000000000000000';
  const res = await fetch('https://polygon-rpc.com/', {
    method: 'POST',
    body: JSON.stringify({
      jsonrpc: '2.0',
      id: 3,
      method: 'eth_call',
      params: [
        {
          from: '0x0000000000000000000000000000000000000000',
          data: data.slice(0, -hex.length) + hex,
          to: '0xfd257ddf743da7395470df9a0517a2dfbf66d156',
        },
        'latest',
      ]},
    ),
  });
  const json = await res.json();
  return Number(json.result);
}

document.querySelector('form').addEventListener('submit', async (e) => {
  e.preventDefault();
  e.target.elements.tokens.value = 'LOADING';
  const tokenId = Number(e.target.elements.tokenId.value);
  const tokens = await getTokens(tokenId);
  const value = Number.isNaN(tokens) ? 'ERROR' : tokens + ' DEFY';
  e.target.elements.tokens.value = value;
});
